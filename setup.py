from setuptools import setup

setup(
    name='hls-m3u8-downloader',
    version='0.2.2',
    description='A simple Python script to download HLS streams.',
    author='Wodok',
    url='https://gitlab.com/wodok/hls-m3u8-downloader',
    license='MIT',
    install_requires=[
        'm3u8',
        'requests',
    ],
)