import contextlib
import sys
import threading
import queue
import m3u8
import requests
import shutil
import tempfile
import os
import posixpath
import urllib.parse
import re
import time
from datetime import datetime, timedelta
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry
import logging
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

keep_temp_files = False

def is_url(uri):
	return re.match(r'https?://', uri) is not None

class DownloadRequest:
	def __init__(self, number, base_uri, uri, path, key):
		self.number = number
		self.base_uri = base_uri
		self.uri = uri
		self.path = path
		self.key = key

class SegmentDownloadThread(threading.Thread):
	def __init__(self, downloadqueue, total):
		threading.Thread.__init__(self)
		self.downloadQueue = downloadqueue
		self.total = total
		session = requests.Session()
		retry = Retry(total=5, backoff_factor=0.5)
		adapter = HTTPAdapter(max_retries=retry)
		session.mount('http://', adapter)
		session.mount('https://', adapter)
		self.session = session
	
	def run(self):
		while True:
			item = self.downloadQueue.get()
			if item is None:
				return
				
			self.execute(item)
			self.downloadQueue.task_done()
	
	def execute(self, item):
		if item.base_uri:
			url = item.base_uri + "/" + item.uri
		else:
			url = item.uri
			item.uri = os.path.basename(urllib.parse.urlparse(url).path)

		# Key is None for our application	
		#if item[3]:
		#	backend = default_backend()
		#	r = self.session.get(item[3].uri, verify=False)
		#	key = r.content
		#	cipher = Cipher(algorithms.AES(key), modes.CBC(bytes.fromhex(item[3].iv[2:])), backend=backend)
		#	decryptor = cipher.decryptor()
		
		try:
			r = self.session.get(url, stream=True, verify=False)
			with open(item.path, 'wb') as f:
				for chunk in r.iter_content(chunk_size=1024):
					if chunk:
						#if item[3]:
						#	f.write(decryptor.update(chunk))
						#else:
						f.write(chunk)
		except:
			logging.warn(f"ERROR downloading {item.uri}")

def concatenate_files(filelist, output_file):
	with open(output_file, 'ab') as outfile:
		for file in filelist:
			if os.path.exists(file) and os.path.getsize(file) >= 10000:
				with open(file, 'rb') as readfile:
					shutil.copyfileobj(readfile, outfile)
			else:
				logging.error(f'ERROR {file}')
	
	outfile.close()

def m3u8_load(uri, session):
	r = session.get(uri, verify=False)
	m3u8_obj = m3u8.M3U8(r.text)
	return m3u8_obj

def hls_fetch(playlist_location, output_file, end):
	session = requests.Session()
	retry = Retry(total=None, backoff_factor=0.5)
	adapter = HTTPAdapter(max_retries=retry)
	session.mount('http://', adapter)
	session.mount('https://', adapter)

	oldSegmentList = list()
	
	if keep_temp_files and not os.path.exists('temp'):
		os.makedirs('temp')
	
	while datetime.now() < end:
		try:
			download_queue = queue.Queue()
			tsSliceList = list()
			
			with (
				tempfile.TemporaryDirectory()
				if not keep_temp_files
				else contextlib.nullcontext('temp')
				) as download_location:
				playlist = m3u8_load(playlist_location, session)
				parsed_url = urllib.parse.urlparse(playlist_location)
				prefix = parsed_url.scheme + '://' + parsed_url.netloc
				base_path = posixpath.normpath(parsed_url.path + '/..')
				base_uri = urllib.parse.urljoin(prefix, base_path)
				pool = list()
				total = 0
				
				for number, file in enumerate(playlist.segments):
					if not is_url(file.uri):
						playlist.base_uri = base_uri
					
					if file.uri not in oldSegmentList:
						total += 1
						logging.debug('new ' + file.uri)
						oldSegmentList.append(file.uri)
						newSegment = os.path.join(download_location, file.uri)
						tsSliceList.append(newSegment)
						download_queue.put(DownloadRequest(number, playlist.base_uri, file.uri, newSegment, file.key))
						
				if total == 0:
					time.sleep(3)
					continue
				
				for i in range(total):
					thread = SegmentDownloadThread(download_queue, total)
					thread.daemon = True
					thread.start()
					pool.append(thread)
				download_queue.join()
				
				for i in range(total):
					download_queue.put(None)
				
				for thread in pool:
					thread.join()
					
				concatenate_files(tsSliceList, output_file)
				tsSliceList.clear()

		except KeyboardInterrupt:
			logging.warn("Ctrl+C pressed. Exiting gracefully.")
			break

		except Exception as e:
			logging.error(e, exc_info=True)

if __name__ == "__main__":
	
	test_url = sys.argv[1]
	output_file = sys.argv[2]
	end = datetime.now() + timedelta(minutes=int(sys.argv[3]))

	hls_fetch(test_url, output_file, end)
